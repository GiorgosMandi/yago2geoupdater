package com.tests;

import java.sql.Connection;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.HashSet;
import java.util.Hashtable;
import java.util.Set;

public class LoadYagoGeos {
    private Connection base_con;

    LoadYagoGeos(Connection a, Hashtable<String[],String> cat_tags) throws SQLException {
        this.base_con=a;
        Statement stmt = this.base_con.createStatement();
        Set<String> value_set = new HashSet<String>(cat_tags.values());
        String sql="UPDATE yago_all_data SET geom_prev=(SELECT geom_ FROM yago_geos WHERE yago_all_data.geometry=yago_geos.id);";
        stmt.executeUpdate(sql);
//        for(String osm_name:value_set){
//            String sql="UPDATE "+osm_name+"_yago SET geom_prev=(SELECT geom_ FROM yago_geos WHERE "+osm_name+"_yago.geometry=yago_geos.id);";
//            stmt.executeUpdate(sql);
//
//        }
        stmt.close();
    }
}
