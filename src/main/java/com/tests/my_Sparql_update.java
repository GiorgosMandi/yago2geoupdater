package com.tests;

import org.apache.http.HttpEntity;
import org.apache.http.client.HttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.xerces.impl.dv.util.Base64;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.conn.ClientConnectionManager;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.impl.conn.PoolingClientConnectionManager;
import org.apache.http.message.BasicNameValuePair;
import org.openrdf.http.client.SesameClientImpl;
import org.openrdf.http.client.util.HttpClientBuilders;
import org.openrdf.model.Value;
import org.openrdf.query.*;
import org.openrdf.repository.RepositoryConnection;
import org.openrdf.repository.http.HTTPRepository;
import org.openrdf.repository.sail.SailRepository;
import org.openrdf.repository.sail.SailRepositoryConnection;
import org.openrdf.repository.sparql.SPARQLRepository;
import org.openrdf.sail.helpers.SailBase;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.nio.charset.Charset;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

public class my_Sparql_update {

    Statement stmt;
    private Connection base_connection;

    String pre_type="http://www.w3.org/1999/02/22-rdf-syntax-ns#type";
    String pre_id="http://kr.di.uoa.gr/yago2geo/ontology/hasOSM_ID";
    String pre_name="http://kr.di.uoa.gr/yago2geo/ontology/hasOSM_Name";
    String pre_geo="http://www.opengis.net/ont/geosparql#hasGeometry";
    String pre_as_wkt="http://www.opengis.net/ont/geosparql#asWKT";
    String ykr="http://yago-knowledge.org/resource/";

    String unwanted_str="\"<http://www.opengis.net/def/crs/EPSG/0/4326>";
    String unwanted_str2="\"^^<http://www.opengis.net/ont/geosparql#wktLiteral>";

    my_Sparql_update(Connection con) throws SQLException, IOException {
        this.base_connection=con;
        stmt = base_connection.createStatement();
        String endpointURL = "http://localhost:8080/strabon-endpoint/Query";
        SPARQLRepository sparqlEndpoint = new SPARQLRepository(endpointURL,"http://localhost:8080/strabon-endpoint/Update");
        //HTTPRepository HTTPEndpoint = new HTTPRepository(endpointURL,"");
        System.out.println(sparqlEndpoint.toString());
        sparqlEndpoint.setUsernameAndPassword("endpoint","3ndpo1nt");
//        SesameClientImpl client = new SesameClientImpl();
//        client.setHttpClientBuilder(HttpClientBuilders.getSSLTrustAllHttpClientBuilder());
//        sparqlEndpoint.setSesameClient(client);
        sparqlEndpoint.initialize();
        String query="SELECT * \n"+
                "WHERE { \n" +
                "?s ?p  ?o.\n" +
                "FILTER (?s = <http://yago-knowledge.org/resource/geoentity_Kapucijnenbos_2794705>)\n" +
                "}\n" ;
        String q2_update="DELETE {<http://yago-knowledge.org/resource/geoentity_Kapucijnenbos_2794705> <http://kr.di.uoa.gr/yago2geo/ontology/hasOSM_Name> \"FOO4\".}\n"+
                "WHERE  {<http://yago-knowledge.org/resource/geoentity_Kapucijnenbos_2794705> <http://kr.di.uoa.gr/yago2geo/ontology/hasOSM_Name> ?t.}";
// Open a connection to this repository
//        System.out.println(q2_update);

        RepositoryConnection con2 =  sparqlEndpoint.getConnection();
        //con2.prepareBooleanQuery(QueryLanguage.SPARQL, q2_update).evaluate();
        //RepositoryConnection con3 =  HTTPEndpoint.getConnection();
//        Update update_query = con2.prepareUpdate(QueryLanguage.SPARQL, q2_update);
//        update_query.execute();

        con.setAutoCommit(false);
        Statement st = con.createStatement();

// Turn use of the cursor on.
        String pattern = ".*[(),.].*";
        st.setFetchSize(3);
        String sql="SELECT id,name,name_updated,link,geometry,category,category_updated,ST_AsText(geom_updated,17) FROM yago_all_data WHERE name_updated is not null or geom_updated is not null or category_updated is not null"; //LIMIT 3
        ResultSet rs = st.executeQuery(sql);
        int counter=0;
        String my_pref="PREFIX ykr: <http://yago-knowledge.org/resource/>\n" +
                "PREFIX geosp:<http://www.opengis.net/ont/geosparql#>\n"+
                "PREFIX rdf:<http://www.w3.org/1999/02/22-rdf-syntax-ns#>\n"+
                "PREFIX yrec:<http://kr.di.uoa.gr/yago2geo/resource/>\n"+
                "PREFIX yont: <http://kr.di.uoa.gr/yago2geo/ontology/>\n";
        String my_query="";
        String my_del="DELETE {";
        String my_ins="INSERT {";
        String my_where="WHERE { ?s ?p ?o.\n"+
                "?o geosp:asWKT ?t.\n"+
                "FILTER(";
        HttpClient hc = HttpClients.createDefault();
        String userPass = "endpoint"+":"+"3ndpo1nt";
        String encoding = Base64.encode(userPass.getBytes());
        while (rs.next()) {
            counter++;
            String name=rs.getString("name");
            String name_updated=rs.getString("name_updated");
            String link=rs.getString("link");
            String category_updated=rs.getString("category_updated");
            String category=rs.getString("category");
            String geometry=rs.getString("geometry");
            String geom_updated=rs.getString("st_astext");
            System.out.println(name+" "+name_updated+" "+category_updated);
            String s_name=null;
            if(link.matches(pattern)){
                my_where=my_where+" ?s= <http://yago-knowledge.org/resource/"+link+"> ||";
                s_name="<http://yago-knowledge.org/resource/"+link+">";
            }
            else{
                my_where=my_where+" ?s= ykr:"+link+" ||";
                s_name="ykr:"+link;
            }

            if(name_updated!=null && name!=null){
                my_del=my_del+s_name+" yont:hasOSM_Name "+name+".\n";
            }



            if(name_updated!=null){
                my_ins=my_ins+s_name+" yont:hasOSM_Name \""+name_updated+"\".\n";
            }
            if(category_updated!=null){
                my_del=my_del+s_name+" rdf:type yont:"+category.replace("osm","OSM")+".\n";
                my_ins=my_ins+s_name+" rdf:type yont:"+category_updated.replace("osm","OSM")+".\n";
            }
            if(geom_updated!=null){
                my_del=my_del+"yrec:"+geometry+" geosp:asWKT ?t.\n";
                my_ins=my_ins+"yrec:"+geometry+" geosp:asWKT "+unwanted_str+" "+geom_updated+unwanted_str2+".\n";
            }
            //my_del=my_del+"ykr:"+geometry+" yont:hasOSM_Name "+name+".\n";

            if(counter==3){
                my_ins=my_ins+"}";
                my_del=my_del+"}";
                my_where=my_where.substring(0,my_where.lastIndexOf(" "));
                my_where=my_where+")\n}";
                System.out.println("=========================================================================================");
                System.out.println(my_pref+"\n"+my_del+"\n"+my_ins+"\n"+my_where);
//                System.out.println(my_del);
//                System.out.println(my_ins);
//                System.out.println(my_where);

                // HttpClient endpoint = new HttpClient("localhost", 8080, "strabon-endpoint");
                HttpPost method = new HttpPost("http://localhost:8080/strabon-endpoint/Update");
                // set the query parameter
                List<NameValuePair> params = new ArrayList<NameValuePair>();
                params.add(new BasicNameValuePair("query", my_pref+"\n"+my_del+"\n"+my_ins+"\n"+my_where));
                UrlEncodedFormEntity encodedEntity = new UrlEncodedFormEntity(params, Charset.defaultCharset());
                method.setEntity(encodedEntity);
                // set the content type
                method.setHeader("Content-Type", "application/x-www-form-urlencoded");
                // set the accept format
                method.addHeader("Accept", "text/xml");
                //set username and password
                //if (getUser()!=null && getPassword()!=null){

                method.setHeader("Authorization", "Basic "+ encoding);
                //}
                try {
                    // response that will be filled next
                    // execute the method
                    HttpResponse response = hc.execute(method);
                    int statusCode = response.getStatusLine().getStatusCode();
                    System.out.println(response.getStatusLine().getReasonPhrase());
                    String responseBody = "";
                    HttpEntity entity = response.getEntity();
                    if (entity != null) {
                        InputStream instream = entity.getContent();
                        try {

                            BufferedReader reader = new BufferedReader(new InputStreamReader(instream));
                            StringBuffer strBuf = new StringBuffer();

                            // do something useful with the response
                            String nextLine;
                            while ((nextLine = reader.readLine()) != null) {
                                strBuf.append(nextLine + "\n");
                            }

                            // remove last newline character
                            if (strBuf.length() > 0) {
                                strBuf.setLength(strBuf.length() - 1);
                            }

                            responseBody = strBuf.toString();

                        } catch (IOException ex) {
                            // In case of an IOException the connection will be released
                            // back to the connection manager automatically
                            throw ex;

                        } catch (RuntimeException ex) {
                            // In case of an unexpected exception you may want to abort
                            // the HTTP request in order to shut down the underlying
                            // connection and release it back to the connection manager.
                            method.abort();
                            throw ex;

                        } finally {
                            // Closing the input stream will trigger connection release
                            instream.close();
                        }
                    }
                    if (statusCode==200) {
                        System.out.println("PETYXE");
                        System.out.println(responseBody);
                    }
                    else{
                        //System.out.println(response.getStatusLine().);
                        System.out.println(responseBody);
                        System.err.println("Status code " + statusCode);
                        System.exit(1);
                        //return false;
                    }
                } catch (IOException e) {
                    throw e;
                } finally {
                    // release the connection.
                    method.releaseConnection();
                }
                my_del="DELETE {";
                my_ins="INSERT {";
                my_where="WHERE { ?s ?p ?o.\n"+
                        "?o geosp:asWKT ?t.\n"+
                        "FILTER(";
                counter=0;
                System.out.println("TELOS");
            }
        }
        rs.close();

        ///////////////////////////////////////////////////////

//        HttpClient hc = HttpClients.createDefault();;
//       // HttpClient endpoint = new HttpClient("localhost", 8080, "strabon-endpoint");
//        HttpPost method = new HttpPost("http://localhost:8080/strabon-endpoint/Update");
//        // set the query parameter
//        List<NameValuePair> params = new ArrayList<NameValuePair>();
//        params.add(new BasicNameValuePair("query", q2_update));
//        UrlEncodedFormEntity encodedEntity = new UrlEncodedFormEntity(params, Charset.defaultCharset());
//        method.setEntity(encodedEntity);
//        // set the content type
//        method.setHeader("Content-Type", "application/x-www-form-urlencoded");
//        // set the accept format
//        method.addHeader("Accept", "text/xml");
//        //set username and password
//        //if (getUser()!=null && getPassword()!=null){
//        String userPass = "endpoint"+":"+"3ndpo1nt";
//        String encoding = Base64.encode(userPass.getBytes());
//        method.setHeader("Authorization", "Basic "+ encoding);
        //}
//        try {
//            // response that will be filled next
//            // execute the method
//            HttpResponse response = hc.execute(method);
//            int statusCode = response.getStatusLine().getStatusCode();
//            System.out.println(response.getStatusLine().getReasonPhrase());
//            String responseBody = "";
//            HttpEntity entity = response.getEntity();
//            if (entity != null) {
//                InputStream instream = entity.getContent();
//                try {
//
//                    BufferedReader reader = new BufferedReader(new InputStreamReader(instream));
//                    StringBuffer strBuf = new StringBuffer();
//
//                    // do something useful with the response
//                    String nextLine;
//                    while ((nextLine = reader.readLine()) != null) {
//                        strBuf.append(nextLine + "\n");
//                    }
//
//                    // remove last newline character
//                    if (strBuf.length() > 0) {
//                        strBuf.setLength(strBuf.length() - 1);
//                    }
//
//                    responseBody = strBuf.toString();
//
//                } catch (IOException ex) {
//                    // In case of an IOException the connection will be released
//                    // back to the connection manager automatically
//                    throw ex;
//
//                } catch (RuntimeException ex) {
//                    // In case of an unexpected exception you may want to abort
//                    // the HTTP request in order to shut down the underlying
//                    // connection and release it back to the connection manager.
//                    method.abort();
//                    throw ex;
//
//                } finally {
//                    // Closing the input stream will trigger connection release
//                    instream.close();
//                }
//            }
//            if (statusCode==200) {
//                System.out.println("PETYXE");
//                System.out.println(responseBody);
//            }
//            else{
//                //System.out.println(response.getStatusLine().);
//                System.out.println(responseBody);
//                System.err.println("Status code " + statusCode);
//                //return false;
//            }
//        } catch (IOException e) {
//            throw e;
//        } finally {
//            // release the connection.
//            method.releaseConnection();
//        }

        //////////////////////////////////////////////////////
        /////////////////////////////////////////////////////


//        TupleQuery tupleQuery = con2.prepareTupleQuery(QueryLanguage.SPARQL, query);
//        TupleQueryResult result = tupleQuery.evaluate();
//        while (result.hasNext()) {
//            BindingSet bindingSet = result.next();
//            System.out.println(bindingSet.toString());
////            Value firstValue = bindingSet.getValue(bindingNames.get(0));
////            Value secondValue = bindingSet.getValue(bindingNames.get(1));
//        }


        //con2.close();
    }
}


//        con.setAutoCommit(false);
//        Statement st = con.createStatement();

// Turn use of the cursor on.
//        st.setFetchSize(20);
//        String sql="SELECT id,name,link,ST_AsText(geom_updated) FROM osm_lake_yago WHERE geom_updated is not null";
//        ResultSet rs = st.executeQuery(sql);
//        int counter=0;
//        while (rs.next()) {
//            counter++;
//            System.out.println(rs.getString("link")+" "+rs.getString("name")+" "+rs.getString("ST_AsText"));
//            if(counter==20){
//                System.out.println("=========================================================================================");
//                counter=0;
//            }
//        }
//        rs.close();
