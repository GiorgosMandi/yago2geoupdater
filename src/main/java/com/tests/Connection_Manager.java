package com.tests;

import org.postgresql.util.PSQLException;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.Statement;
import java.util.LinkedHashMap;
import java.util.Scanner;

public class Connection_Manager {
    private String base_name;
    private String base_url;
    private String base_user;
    private String base_password;
    private Connection c;


    public Connection_Manager(LinkedHashMap<String, String> base_info) {
        this.base_name = base_info.get("base_name");  // Set the initial value for the class attribute x
        this.base_url=base_info.get("base_url");
        this.base_user=base_info.get("base_user");
        this.base_password=base_info.get("base_password");
        this.c = null;
        try{
            Class.forName("org.postgresql.Driver");
            this.c = DriverManager.getConnection(this.base_url,this.base_user, this.base_password);
        }
        catch (PSQLException e) {
//            e.printStackTrace();
            if(e.getErrorCode()==0){
                System.out.println(e.getErrorCode()+": "+e.getMessage());

                String s;
                do{
                    System.out.println("Do you want to create the named Database?Please type (yes)|(no)");
                    Scanner scan = new Scanner(System.in);
                    s = scan.next();
                }while(!s.equalsIgnoreCase("yes") && !s.equalsIgnoreCase("no"));


                if(s.equalsIgnoreCase("yes")){
                    try {
                        Connection c2;
                        c2 = DriverManager.getConnection(this.base_url.replace(this.base_name,""), this.base_user, this.base_password);
                        Statement statement = c2.createStatement();
                        statement.executeUpdate("CREATE DATABASE "+this.base_name);
//                        statement.executeQuery("CREATE EXTENSION postgis;");
                        this.c = DriverManager.getConnection(this.base_url,this.base_user, this.base_password);
                        Statement stmt=this.c.createStatement();
                        stmt.executeUpdate("CREATE EXTENSION IF NOT EXISTS postgis;");
                    }
                    catch (Exception e2) {
                        e2.printStackTrace();
                        System.err.println(e2.getClass().getName()+": "+e2.getMessage());
                        System.exit(10);
                    }
                }
                else{
                    System.exit(10);
                }
            }

//            System.exit(10);
        }
        catch (Exception e2) {
            e2.printStackTrace();
            System.err.println(e2.getClass().getName()+": "+e2.getMessage());
            System.exit(10);
        }

    }

    public Connection get_Connection(){
        return c;
    }



}
