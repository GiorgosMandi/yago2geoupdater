package com.tests;

public class Way_node {

    Way_node(long _id,String _name,String _shape, String _geometry){
        this.id=_id;
        this.name=_name;
        this.shape=_shape;
        this.geometry=_geometry;
    }
    public long id;
    public String name;
    public String shape;
    public String geometry;
}
